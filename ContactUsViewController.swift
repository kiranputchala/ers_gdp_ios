//
//  ContactUsViewController.swift
//  ERS_GDP_01
//
//  Created by Venkata Naga Kiran Putchala on 3/2/17.
//  Copyright © 2017 Madishetty,Sumesh. All rights reserved.
//

import UIKit

class ContactUsViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Contact Us"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
