//
//  SignInViewController.swift
//  ERS_GDP_01
//
//  Created by Venkata Naga Kiran Putchala on 12/9/16.
//  Copyright © 2016 Madishetty,Sumesh. All rights reserved.
//

import UIKit
import Parse
import Bolts

class SignInViewController: UIViewController, UITextFieldDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        userNameTF.delegate = self
        passwordTF.delegate = self

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        let myStory: UIStoryboard = UIStoryboard(name:"Main", bundle: nil)
        let currentUser = PFUser.currentUser()
        if currentUser != nil {
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                let userHome: UINavigationController  =
                    myStory.instantiateViewControllerWithIdentifier("homepage") as! UINavigationController
                self.showViewController(userHome, sender: PFUser())
            })
        }
        else {
            PFUser.logOut()
            let signIn: SignInViewController  =
                myStory.instantiateViewControllerWithIdentifier("loginpage") as! SignInViewController
            self.showViewController(signIn, sender: PFUser())
        }
    }
    
  
    @IBOutlet weak var userNameTF: UITextField!
    
    
    @IBOutlet weak var passwordTF: UITextField!
    
    
    @IBAction func loginBTN(sender: AnyObject) {
        
        let userName = userNameTF!.text
        let userPassword = passwordTF!.text
        if userName!.isEmpty || userPassword!.isEmpty {
            displayAlertWithTitle("Oops!!", message: "All Fields are Required")
            return
        }
        PFUser.logInWithUsernameInBackground(userNameTF.text!, password: passwordTF.text!, block: {(user, error) -> Void in
            
            if error != nil{
                print(error)
                self.displayAlertWithTitle("Login Failed", message:"Username/Password does not match")
            }
            else {
                
                let emailVerified = user!["emailVerified"]
                if emailVerified != nil && (emailVerified as! Bool) == true {
                    print("Login Success")
                    let mystory:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let userHome: UINavigationController =
                        mystory.instantiateViewControllerWithIdentifier("homepage") as! UINavigationController
                    self.showViewController(userHome, sender: user)
                }
                else {
                    // The email has not been verified, so logout the user
                    self.displayAlertWithTitle("Login Failed", message:"Please verify your Email Id")
                    PFUser.logOut()
                }
            }
        })
    }
    
    @IBAction func unwindSegueLogout(sender: UIStoryboardSegue){
        
        userNameTF!.text = ""
        passwordTF!.text = ""
        if PFUser.currentUser() != nil {
            PFUser.logOut()
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
    }
    
    func displayAlertWithTitle(title:String, message:String){
        let alert:UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        let defaultAction:UIAlertAction =  UIAlertAction(title: "OK", style: .Default, handler: nil)
        alert.addAction(defaultAction)
        self.presentViewController(alert, animated: true, completion: nil)
    }
}
