//
//  ViewController.swift
//  ERS_GDP_01
//
//  Created by Madishetty,Sumesh on 10/9/16.
//  Copyright © 2016 Madishetty,Sumesh. All rights reserved.
//

import UIKit
import CoreLocation
import Parse

class ViewController: UIViewController, CLLocationManagerDelegate {
    
    let currentUser = PFUser.currentUser()
    let locationManager = CLLocationManager()
    var userAddress = ""
    var userDetails = ""
    var longitude = 0.0
    var latitude = 0.0
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Home"
        self.userAddress = ""
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func sendMailBTN(sender: AnyObject) {
        print("Button Clicked")
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        CLGeocoder().reverseGeocodeLocation(manager.location!) { (placemarks, error) in
            
            if error != nil {
                print("Error: " + (error?.localizedDescription)!)
            }
            
            if placemarks?.count > 0 {
                let pm = placemarks![0] as! CLPlacemark
                self.displayLocationInfo(pm)
                print("After fetching location")
                let userLocation:CLLocation = locations[0]
                self.longitude = userLocation.coordinate.longitude
                self.latitude = userLocation.coordinate.latitude
                let emergency = PFObject(className: "ImmediateEmergency")
                print("Inside emergency table")
                emergency["userFirstName"] = self.currentUser!["FirstName"] as! String
                emergency["userLastName"] =  self.currentUser!["LastName"] as! String
                emergency["phoneNumber"] = self.currentUser!["PhoneNumber"] as! String
                emergency["email"] = self.currentUser!.email!
                emergency["type"] = "Immediate Emergency"
                emergency["location"] = self.userAddress
                emergency.saveInBackgroundWithBlock({ (success, error) -> Void in
                    if success {
                        self.sendMail(emergency,lat: self.latitude,long: self.longitude)
                        self.displayAlertWithTitle("Success!",message:"Emergency Reported")
                    } else {
                        print(error)
                    }
                })
            }
            self.userDetails = "<p>Name: \(self.currentUser!["FirstName"]) \(self.currentUser!["LastName"])</p><p>\nEmergency Type: Immediate\nLocation: \(self.userAddress)</p><p>\n919#: \(self.currentUser!["NineNumber"])</p><p>\nEmergency Contact Name: \(self.currentUser!["emerConName"])</p><p>\nEmergency Contact Phone: \(self.currentUser!["emerConPhone"])</p><p>\nRelation with Emergency Contact: \(self.currentUser!["emerConRel"])</p>"
            print(self.userDetails)
        }
    }
    
    func sendMail(sender: AnyObject,lat: Double,long:Double) {
        let smtpSession = MCOSMTPSession()
        smtpSession.hostname = "smtp.gmail.com"
        smtpSession.username = "emergencyreportappios@gmail.com"
        smtpSession.password = "xysbwltshmxrpcbe"
        smtpSession.port = 465
        smtpSession.authType = MCOAuthType.SASLPlain
        smtpSession.connectionType = MCOConnectionType.TLS
        smtpSession.connectionLogger = {(connectionID, type, data) in
            if data != nil {
                if let string = NSString(data: data, encoding: NSUTF8StringEncoding){
                    NSLog("Connectionlogger: \(string)")
                }
            }
        }
        
        let builder = MCOMessageBuilder()
        builder.header.to = [MCOAddress(displayName: "UPD" , mailbox: "venkatkiran999@gmail.com")]
        builder.header.from = MCOAddress(displayName: "Emergency Reporting App", mailbox: "emergencyreportappios@gmail.com")
        builder.header.subject = "Emergency Reporting System Need Immediate Attention"
        builder.htmlBody = "User Address: \(self.userAddress) User details<p>\(self.userDetails) <p>User Location. Click on the link to User location and easy navigation</p> <a href=\"https://www.google.com/maps/@\(lat),\(long),20z\">Maps</a>"
        
        let rfc822Data = builder.data()
        let sendOperation = smtpSession.sendOperationWithData(rfc822Data)
        sendOperation.start { (error) -> Void in
            if (error != nil) {
                NSLog("Error sending email: \(error)")
            } else {
                NSLog("Successfully sent email!")
            }
        }
    }
    
    
    func displayLocationInfo(placemark: CLPlacemark) {
        self.locationManager.stopUpdatingLocation()
        let myAddress = "\(placemark.subThoroughfare!) \(placemark.thoroughfare!) \(placemark.locality!) \(placemark.country!) \(placemark.postalCode!)"
        self.userAddress = myAddress
        print(self.userAddress)
    }
    
    func displayAlertWithTitle(title:String, message:String){
        let alert:UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        let defaultAction:UIAlertAction =  UIAlertAction(title: "OK", style: .Default, handler: nil)
        alert.addAction(defaultAction)
        self.presentViewController(alert, animated: true, completion: nil)
        
    }
}

