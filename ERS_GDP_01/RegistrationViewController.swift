//
//  RegistrationViewController.swift
//  ERS_GDP_01
//
//  Created by Venkata Naga Kiran Putchala on 12/9/16.
//  Copyright © 2016 Madishetty,Sumesh. All rights reserved.
//

import UIKit
import Parse

class RegistrationViewController: UIViewController, UITextFieldDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        firstNameTF.delegate = self
        lastNameTF.delegate = self
        userNameTF.delegate = self
        nineNumTF.delegate = self
        nineNumTF.keyboardType = UIKeyboardType.NumberPad
        emailTF.delegate = self
        addressLineOneTF.delegate = self
        addressLineTwoTF.delegate = self
        phoneNumTF.delegate = self
        passwordTF.delegate = self
        cfmPasswordTF.delegate = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var firstNameTF: UITextField!
    
    @IBOutlet weak var lastNameTF: UITextField!
    
    @IBOutlet weak var userNameTF: UITextField!
    
    @IBOutlet weak var nineNumTF: UITextField!
    
    @IBOutlet weak var emailTF: UITextField!
    
    @IBOutlet weak var addressLineOneTF: UITextField!
    
    @IBOutlet weak var addressLineTwoTF: UITextField!
    
    @IBOutlet weak var phoneNumTF: UITextField!
    
    @IBOutlet weak var passwordTF: UITextField!
    
    @IBOutlet weak var cfmPasswordTF: UITextField!
    
    @IBAction func regPageCloseBTN(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func showPwdReq(sender: AnyObject) {
        
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("pwdPopUp") as! PwdReqPopUpViewController
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        self.view.addSubview(popOverVC.view)
        popOverVC.didMoveToParentViewController(self)
    }
    
    @IBAction func registerBTN(sender: AnyObject) {
        
        let userName = userNameTF!.text
        let firstName = firstNameTF!.text
        let userLasName = lastNameTF!.text
        let nineNum = nineNumTF!.text
        let email = emailTF!.text
        let address1 = addressLineOneTF!.text
        let address2 = addressLineTwoTF!.text
        let phone = phoneNumTF!.text
        let password = passwordTF!.text
        
        if firstNameTF!.text!.isEmpty || lastNameTF!.text!.isEmpty || emailTF!.text!.isEmpty || addressLineOneTF!.text!.isEmpty || phoneNumTF!.text!.isEmpty || userNameTF!.text!.isEmpty || passwordTF!.text!.isEmpty || cfmPasswordTF!.text!.isEmpty {
            displayMyAlertMessage("Fill in the details")
            return
        }
        
        if passwordTF.text?.characters.count < 8 {
            displayMyAlertMessage("Password should be a minimum of 8 characters")
            return
        }
        
        let passwordRegex = "^(?=.*?[A-Z]).{8,}$"
        let passwordTest = NSPredicate(format:"SELF MATCHES %@", passwordRegex)
        if passwordTest.evaluateWithObject(password) {
            // Do nothing continue forward
        }
        else {
            displayMyAlertMessage("Please enter a valid password")
            return
        }
        
        if passwordTF!.text! != cfmPasswordTF!.text! {
            displayMyAlertMessage("Passwords don't match")
            return
        }
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        if emailTest.evaluateWithObject(email) {
            // Do nothing continue forward
        }
        else {
            //emailIdValidation = false
            displayMyAlertMessage("Please enter valid EmailID")
            return
        }
        
        let PHONE_REGEX = "^\\d{3}\\d{3}\\d{4}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        if phoneTest.evaluateWithObject(phone) {
            // Do nothing continue forward
        }
        else {
            displayMyAlertMessage("Please enter valid phone number")
            return
        }
        
        let myuser:PFUser = PFUser()
        myuser.username = userName
        myuser.password = password
        myuser.email = email
        myuser.setObject(firstName!, forKey: "FirstName")
        myuser.setObject(userLasName!, forKey: "LastName")
        if nineNum!.isEmpty {
            //Do Nothing
        } else {
            myuser.setObject(nineNum!, forKey: "NineNumber")
        }
        myuser.setObject(address1!, forKey: "AddressLine1")
        if address2!.isEmpty {
            //Do Nothing
        }else{
            myuser.setObject(address2!, forKey: "AddressLine2")
        }
        myuser.setObject(phone!, forKey: "PhoneNumber")
        
        myuser.signUpInBackgroundWithBlock( {
            (success, error) -> Void in
            if let error = error as NSError? {
                let errorString = error.userInfo["error"] as? NSString
                // In case something went wrong, use errorString to get the error
                self.displayAlertWithTitle("Something has gone wrong", message:"\(errorString)")
            } else {
                // Everything went okay
                self.displayAlertWithTitle("Success!", message:"Your Registration is Successful. Please verify your email ID")
                //Empty all the Fields
                self.userNameTF!.text = ""
                self.firstNameTF!.text = ""
                self.lastNameTF!.text = ""
                self.nineNumTF!.text = ""
                self.emailTF!.text = ""
                self.addressLineOneTF!.text = ""
                self.addressLineTwoTF!.text = ""
                self.phoneNumTF!.text = ""
                self.passwordTF!.text = ""
                self.cfmPasswordTF!.text = ""
                
                let emailVerified = myuser["emailVerified"]
                if emailVerified != nil && (emailVerified as! Bool) == true {
                    // Everything is fine
                    
                } else {
                    // The email has not been verified, so logout the user
                    PFUser.logOut()
                }
            } })
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
    }
    
    func displayMyAlertMessage(userMessage:String) {
        
        let myAlert = UIAlertController(title:"Alert", message:userMessage, preferredStyle: UIAlertControllerStyle.Alert)
        
        let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil)
        
        myAlert.addAction(okAction)
        self.presentViewController(myAlert, animated: true, completion: nil)
        
    }
    
    func displayAlertWithTitle(title:String, message:String){
        let alert:UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        let defaultAction:UIAlertAction =  UIAlertAction(title: "OK", style: .Default, handler: nil)
        alert.addAction(defaultAction)
        self.presentViewController(alert, animated: true, completion: nil)
        
    }

}
