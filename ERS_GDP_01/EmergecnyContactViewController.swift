//
//  EmergecnyContactViewController.swift
//  ERS_GDP_01
//
//  Created by Venkata Naga Kiran Putchala on 12/9/16.
//  Copyright © 2016 Madishetty,Sumesh. All rights reserved.
//

import UIKit
import Parse

class EmergecnyContactViewController: UIViewController, UITextFieldDelegate {
    
    let myUser:PFUser = PFUser.currentUser()!
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Add Emergency Contact"
        emergConNameTF.delegate = self
        emergConRelTF.delegate = self
        emergConAddrTF.delegate = self
        emergConPhoneTF.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var emergConNameTF: UITextField!
    
    @IBOutlet weak var emergConRelTF: UITextField!
    
    @IBOutlet weak var emergConAddrTF: UITextField!
    
    @IBOutlet weak var emergConPhoneTF: UITextField!
    
    @IBAction func addEmergencyContact(sender: AnyObject) {
        
        if emergConNameTF!.text!.isEmpty || emergConRelTF.text!.isEmpty || emergConAddrTF!.text!.isEmpty || emergConPhoneTF!.text!.isEmpty {
            displayAlertWithTitle("OOPS!!", message: "Please fill out all the fields")
            return
        }
        
        let PHONE_REGEX = "^\\d{3}\\d{3}\\d{4}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        if phoneTest.evaluateWithObject(emergConPhoneTF.text) {
            // Do nothing continue forward
        }
        else {
            displayAlertWithTitle("Error!", message: "Please enter a valid phone number")
            return
        }
        
        let emerConName = emergConNameTF!.text
        let emerConRelation = emergConRelTF!.text
        let emerConAddress = emergConAddrTF!.text
        let emerConPhone = emergConPhoneTF!.text
        print("Clicked Add Emer Con")
        myUser.setObject(emerConName!, forKey: "emerConName")
        myUser.setObject(emerConRelation!, forKey: "emerConRel")
        myUser.setObject(emerConAddress!, forKey: "emerConAdd")
        myUser.setObject(emerConPhone!, forKey: "emerConPhone")
        myUser.saveInBackgroundWithBlock({ (success, error) -> Void in
            if success {
                self.displayAlertWithTitle("Success!",message:"Emergency Contact Added")
            } else {
                print(error)
            }
        })
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
    }
    
    func displayAlertWithTitle(title:String, message:String){
        let alert:UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        let defaultAction:UIAlertAction =  UIAlertAction(title: "OK", style: .Default, handler: nil)
        alert.addAction(defaultAction)
        self.presentViewController(alert, animated: true, completion: nil)
        
    }

    
}
