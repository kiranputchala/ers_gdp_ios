//
//  ProfileUpdateViewController.swift
//  ERS_GDP_01
//
//  Created by Venkata Naga Kiran Putchala on 12/1/16.
//  Copyright © 2016 Madishetty,Sumesh. All rights reserved.
//

import UIKit
import Parse

class ProfileUpdateViewController: UIViewController, UITextFieldDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "My Profile"
        userFirstNameTF.delegate = self
        userLastNameTF.delegate = self
        userNameTF.delegate = self
        userEmailTF.delegate = self
        userAddress1TF.delegate = self
        userAddress2TF.delegate = self
        phoneNumTF.delegate = self
        userPwdTF.delegate = self
        let currentUser = PFUser.currentUser()
        userFirstNameTF.text! = currentUser!["FirstName"] as! String
        userLastNameTF.text! = currentUser!["LastName"] as! String
        userEmailTF.text! = currentUser!.email!
        userNameTF.text! = currentUser!.username!
        userAddress1TF.text! = currentUser!["AddressLine1"] as! String
        if let str = currentUser!["AddressLine2"] {
            userAddress2TF.text! = str as! String
        }
        phoneNumTF.text! = currentUser!["PhoneNumber"] as! String
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var userFirstNameTF: UITextField!
    
    @IBOutlet weak var userLastNameTF: UITextField!
    
    @IBOutlet weak var userNameTF: UITextField!
    
    @IBOutlet weak var userEmailTF: UITextField!
    
    @IBOutlet weak var userAddress1TF: UITextField!
    
    @IBOutlet weak var userAddress2TF: UITextField!
    
    @IBOutlet weak var phoneNumTF: UITextField!
    
    @IBOutlet weak var userPwdTF: UITextField!
    
    
    @IBAction func showPwdPopUp(sender: AnyObject) {
        
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("pwdPopUp") as! PwdReqPopUpViewController
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        self.view.addSubview(popOverVC.view)
        popOverVC.didMoveToParentViewController(self)
    }
    
    
    @IBAction func updBTN(sender: AnyObject) {
        
        if userFirstNameTF.text!.isEmpty || userLastNameTF.text!.isEmpty || userEmailTF.text!.isEmpty || userNameTF.text!.isEmpty || userAddress1TF.text!.isEmpty || phoneNumTF.text!.isEmpty {
            displayMyAlertMessage("Fields cannot be left blank")
            return
        }
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        if emailTest.evaluateWithObject(userEmailTF.text!) {
            // Do nothing continue forward
        }
        else {
            //emailIdValidation = false
            displayMyAlertMessage("Please enter valid EmailID")
            return
        }

        
        let PHONE_REGEX = "^\\d{3}\\d{3}\\d{4}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        if phoneTest.evaluateWithObject(phoneNumTF.text!) {
            // Do nothing continue forward
        }
        else {
            displayMyAlertMessage("Please enter valid phone number")
            return
        }
        if userPwdTF.text!.isEmpty {
            // Do Nothing 
        }else {
            let passwordRegex = "^(?=.*?[A-Z]).{8,}$"
            let passwordTest = NSPredicate(format:"SELF MATCHES %@", passwordRegex)
            if passwordTest.evaluateWithObject(userPwdTF.text!) {
                // Do nothing continue forward
            }
            else {
                displayMyAlertMessage("Please enter a valid password")
                return
            }
        }
        
        if let user = PFUser.currentUser(){
            
            user["FirstName"] = userFirstNameTF.text!
            user["LastName"] =  userLastNameTF.text!
            user["username"] = userNameTF.text!
            user["email"] = userEmailTF.text!
            user["AddressLine1"] = userAddress1TF.text!
            user["AddressLine2"] = userAddress2TF.text!
            user["PhoneNumber"] = phoneNumTF.text!
            user["password"] = userPwdTF.text!
            user.saveInBackground()
            displayMyAlertMessage("Profile Updated Successfully. A mail has been sent to your registered email ID, Please verify it")
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
    }

    // Function to display the alert messages
    func displayMyAlertMessage(userMessage:String) {
        
        let myAlert = UIAlertController(title:"Alert", message:userMessage, preferredStyle: UIAlertControllerStyle.Alert)
        
        let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil)
        
        myAlert.addAction(okAction)
        self.presentViewController(myAlert, animated: true, completion: nil)
        
    }
    
    
}
