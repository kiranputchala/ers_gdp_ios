//
//  UpdateEmergencyViewController.swift
//  ERS_GDP_01
//
//  Created by Venkata Naga Kiran Putchala on 3/29/17.
//  Copyright © 2017 Madishetty,Sumesh. All rights reserved.
//

import UIKit
import Parse

class UpdateEmergencyViewController: UIViewController, UITextFieldDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Update Emergency Contact"
        emerConNameTF.delegate = self
        emerConRelTF.delegate = self
        emerConAddTF.delegate = self
        emerConPhoneTF.delegate = self
        let currentUser = PFUser.currentUser()
        if let emerConName:String = currentUser!["emerConName"] as? String {
            emerConNameTF.text! = emerConName
        }
        if let emerConRelName:String = currentUser!["emerConRel"] as? String {
            emerConRelTF.text! = emerConRelName
        }
        if let emerConAdd:String = currentUser!["emerConAdd"] as? String {
            emerConAddTF.text! = emerConAdd
        }
        if let emerConPhone:String = currentUser!["emerConPhone"] as? String {
            emerConPhoneTF.text! = emerConPhone
        }
    }
    
    @IBOutlet weak var emerConNameTF: UITextField!
    
    @IBOutlet weak var emerConRelTF: UITextField!
    
    @IBOutlet weak var emerConAddTF: UITextField!
    
    @IBOutlet weak var emerConPhoneTF: UITextField!
    
    @IBAction func emerConUpdBTN(sender: AnyObject){
        
        if emerConNameTF!.text!.isEmpty || emerConRelTF.text!.isEmpty || emerConAddTF!.text!.isEmpty || emerConPhoneTF!.text!.isEmpty {
            displayAlertWithTitle("OOPS!!", message: "Please fill out all the fields")
            return
        }
        
        let PHONE_REGEX = "^\\d{3}\\d{3}\\d{4}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        if phoneTest.evaluateWithObject(emerConPhoneTF.text) {
            // Do nothing continue forward
        }
        else {
            displayAlertWithTitle("Error!", message: "Please enter a valid phone number")
            return
        }
        let emerConName = emerConNameTF!.text
        let emerConRelation = emerConRelTF!.text
        let emerConAddress = emerConAddTF!.text
        let emerConPhone = emerConPhoneTF!.text
        if let user = PFUser.currentUser() {
            user["emerConName"] = emerConName
            user["emerConPhone"] = emerConPhone
            user["emerConRel"] = emerConRelation
            user["emerConAdd"] = emerConAddress
            user.saveInBackground()
            displayAlertWithTitle("Success", message: "Emergency Contact Updated")
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
    }
    // Function to display the alert messages
    func displayAlertWithTitle(title:String, message:String){
        let alert:UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        let defaultAction:UIAlertAction =  UIAlertAction(title: "OK", style: .Default, handler: nil)
        alert.addAction(defaultAction)
        self.presentViewController(alert, animated: true, completion: nil)
        
    }
    
}
