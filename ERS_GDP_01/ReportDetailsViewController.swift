//
//  ReportDetailsViewController.swift
//  ERS_GDP_01
//
//  Created by Venkata Naga Kiran Putchala on 1/30/17.
//  Copyright © 2017 Madishetty,Sumesh. All rights reserved.
//

import UIKit
import CoreLocation
import Parse

class ReportDetailsViewController: UIViewController, CLLocationManagerDelegate, UITextFieldDelegate, UITextViewDelegate {
    
    var emergType: String = ""
    let currentUser = PFUser.currentUser()
    let locationManager = CLLocationManager()
    var userAddress = ""
    var userDetails = ""
    var longitude = 0.0
    var latitude = 0.0

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Report Details"
        emergencyTypeTF!.text = emergType
        emergencyTypeTF.delegate = self
        emergDescripTV.delegate = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
        
    @IBOutlet weak var emergencyTypeTF: UITextField!
    
    @IBOutlet weak var emergDescripTV: UITextView!
    
    
    @IBAction func reportNonImmediateEmergency(sender: AnyObject) {
        print("Button Clicked")
        
        let emerType = emergencyTypeTF.text!
        let emerDesc = emergDescripTV.text!
        if emerType.isEmpty || emerDesc.isEmpty {
            displayAlertWithTitle("Oops!!", message: "Please enter all details")
            return
        }
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
    }
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        CLGeocoder().reverseGeocodeLocation(manager.location!) { (placemarks, error) in
            if error != nil {
                print("Error: " + (error?.localizedDescription)!)
            }
            
            if placemarks?.count > 0 {
                let pm = placemarks![0] as! CLPlacemark
                self.displayLocationInfo(pm)
                print("After fetching location")
                let userLocation:CLLocation = locations[0]
                self.longitude = userLocation.coordinate.longitude
                self.latitude = userLocation.coordinate.latitude
                let emergency = PFObject(className: "Emergency")
                print("Inside Non Emergency table")
                emergency["userFirstName"] = self.currentUser!["FirstName"] as! String
                emergency["userLastName"] =  self.currentUser!["LastName"] as! String
                emergency["phoneNumber"] = self.currentUser!["PhoneNumber"] as! String
                emergency["email"] = self.currentUser!.email!
                emergency["type"] = self.emergencyTypeTF.text!
                emergency["description"] = self.emergDescripTV.text!
                emergency["location"] = self.userAddress
                emergency.saveInBackgroundWithBlock({ (success, error) -> Void in
                    if success {
                        self.sendMail(emergency,lat: self.latitude,long: self.longitude)
                        self.displayAlertWithTitle("Success!",message:"Emergency Reported")
                    } else {
                        print(error)
                    }
                })
            }
            self.userDetails = "<p>Name: \(self.currentUser!["FirstName"]) \(self.currentUser!["LastName"])</p><p>\nEmergency Type: \(self.emergencyTypeTF.text!)</p><p>\nEmergency Description: \(self.emergDescripTV.text!)</p><p>\nLocation: \(self.userAddress)</p><p>\n919#: \(self.currentUser!["NineNumber"])</p><p>\nEmergency Contact Name: \(self.currentUser!["emerConName"])</p><p>\nEmergency Contact Phone: \(self.currentUser!["emerConPhone"])</p><p>\nRelation with Emergency Contact: \(self.currentUser!["emerConRel"])</p>"
            print(self.userDetails)
        }
    }
    
    func sendMail(sender: AnyObject,lat: Double,long:Double) {
        let smtpSession = MCOSMTPSession()
        smtpSession.hostname = "smtp.gmail.com"
        smtpSession.username = "emergencyreportappios@gmail.com"
        smtpSession.password = "xysbwltshmxrpcbe"
        smtpSession.port = 465
        smtpSession.authType = MCOAuthType.SASLPlain
        smtpSession.connectionType = MCOConnectionType.TLS
        smtpSession.connectionLogger = {(connectionID, type, data) in
            if data != nil {
                if let string = NSString(data: data, encoding: NSUTF8StringEncoding){
                    NSLog("Connectionlogger: \(string)")
                }
            }
        }
        
        let builder = MCOMessageBuilder()
        builder.header.to = [MCOAddress(displayName: "UPD" , mailbox: "venkatkiran999@gmail.com")]
        builder.header.from = MCOAddress(displayName: "Emergency Reporting App", mailbox: "emergencyreportappios@gmail.com")
        builder.header.subject = "\(self.emergencyTypeTF.text!) Emergency!! Need Attention"
        builder.htmlBody = self.userAddress
        builder.htmlBody = "User Address: \(self.userAddress) User details<p>\(self.userDetails)</p> <p>User Location. Click on the link below to see User location and easy navigation</p> <a href=\"https://www.google.com/maps/@\(lat),\(long),20z\">Maps</a>"
        
        let rfc822Data = builder.data()
        let sendOperation = smtpSession.sendOperationWithData(rfc822Data)
        sendOperation.start { (error) -> Void in
            if (error != nil) {
                NSLog("Error sending email: \(error)")
            } else {
                NSLog("Successfully sent email!")
            }
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
    }
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    func displayLocationInfo(placemark: CLPlacemark) {
        self.locationManager.stopUpdatingLocation()
        let myAddress = "\(placemark.subThoroughfare!) \(placemark.thoroughfare!) \(placemark.locality!) \(placemark.country!) \(placemark.postalCode!)"
        //print(myAddress)
        self.userAddress = myAddress
        print(self.userAddress)
    }
    
    func displayAlertWithTitle(title:String, message:String){
        let alert:UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        let defaultAction:UIAlertAction =  UIAlertAction(title: "OK", style: .Default, handler: nil)
        alert.addAction(defaultAction)
        self.presentViewController(alert, animated: true, completion: nil)
    }
}
