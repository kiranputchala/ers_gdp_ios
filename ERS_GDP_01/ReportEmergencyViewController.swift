//
//  ReportEmergencyViewController.swift
//  ERS_GDP_01
//
//  Created by Venkata Naga Kiran Putchala on 12/6/16.
//  Copyright © 2016 Madishetty,Sumesh. All rights reserved.
//

import UIKit

class ReportEmergencyViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Report Emergency"
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "earthquake" {
            let reportDetails = segue.destinationViewController as! ReportDetailsViewController
            reportDetails.emergType = "Earth Quake"
        }
        if segue.identifier == "fire" {
            let reportDetails = segue.destinationViewController as! ReportDetailsViewController
            reportDetails.emergType = "Fire"
        }
        if segue.identifier == "drugs" {
            let reportDetails = segue.destinationViewController as! ReportDetailsViewController
            reportDetails.emergType = "Drugs"
        }
        if segue.identifier == "other" {
            let reportDetails = segue.destinationViewController as! ReportDetailsViewController
            reportDetails.emergType = "Other"
        }
        if segue.identifier == "medical" {
            let reportDetails = segue.destinationViewController as! ReportDetailsViewController
            reportDetails.emergType = "Medical"
        }
        if segue.identifier == "weather" {
            let reportDetails = segue.destinationViewController as! ReportDetailsViewController
            reportDetails.emergType = "Weather"
        }
    }
}
